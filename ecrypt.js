const CryptoJS = require("crypto-js");
// user defined modules
const { ENPASS } = require("../env.js");
const IP = require('ip');
const upload_error = require("../node_error_functions/upload_error")
const keySize = 256;
const ivSize = 128;
const iterations = 100;

module.exports = (msg) => {
  try {
  const salt = CryptoJS.lib.WordArray.random(128 / 8);

  const key = CryptoJS.PBKDF2(ENPASS, salt, {
    keySize: keySize / 32,
    iterations: iterations,
  });

  const iv = CryptoJS.lib.WordArray.random(128 / 8);

  const encrypted = CryptoJS.AES.encrypt(msg, key, {
    iv: iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  });

  // salt, iv will be hex 32 in length
  // append them to the ciphertext for use  in decryption
  const transitmessage = salt.toString() + iv.toString() + encrypted.toString();
  return transitmessage;
} catch (err) {
  console.log(err)
    upload_error({
      errorTitle: "Encrypting String",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
}
};
