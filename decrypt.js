const CryptoJS = require("crypto-js");
// user defined modules
const { ENPASS } = require("../env.js");
const IP = require('ip');
const upload_error = require("../node_error_functions/upload_error")
const keySize = 256;
const ivSize = 128;
const iterations = 100;

module.exports = (transitmessage) => {
  try{
  const salt = CryptoJS.enc.Hex.parse(transitmessage.substr(0, 32));
  const iv = CryptoJS.enc.Hex.parse(transitmessage.substr(32, 32));
  const encrypted = transitmessage.substring(64);

  const key = CryptoJS.PBKDF2(ENPASS, salt, {
    keySize: keySize / 32,
    iterations: iterations,
  });

  const decrypted = CryptoJS.AES.decrypt(encrypted, key, {
    iv: iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  }).toString(CryptoJS.enc.Utf8);
  let app_decrypted;
  try {
    app_decrypted = CryptoJS.AES.decrypt(encrypted, ENPASS, {
      iv: iv,
      padding: CryptoJS.pad.Pkcs7,
      mode: CryptoJS.mode.CBC,
    }).toString(CryptoJS.enc.Utf8);
  } catch (err) {
    app_decrypted = "";
  }

  return { decrypted, app_decrypted };
} catch (err) {
  console.log(err)
    upload_error({
      errorTitle: "Decrypting String",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
}
};
